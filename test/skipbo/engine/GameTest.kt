package skipbo.engine

import org.junit.jupiter.api.Test
import skipbo.engine.playarea.DrawPile
import skipbo.engine.player.Player
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

internal class GameTest {
    internal class StartingGame {
        private val rng = Random.Default

        @Test
        fun `should be able to create games`() {
            Game(1, 10)
            Game(1, 30)
            Game(1, 50)

            Game(2, 30)
            Game(3, 30)
            Game(4, 30)

            Game(5, 20)
            Game(6, 20)
        }

        @Test
        fun `should have correct amounts of cards in single player game`() {
            val stockSize = rng.nextInt(99) + 1
            val game = Game(1, stockSize)
            assertEquals(stockSize, game.players[0].stockPile.size)
            assertEquals(DrawPile.TOTAL_CARD_COUNT - stockSize - Player.HAND_SIZE, game.drawPile.size)
        }

        @Test
        fun `should have correct amounts of cards in multiplayer game`() {
            val stockSize = rng.nextInt(39) + 1
            val game = Game(3, stockSize)
            assertEquals(stockSize, game.players[0].stockPile.size)
            assertEquals(stockSize, game.players[1].stockPile.size)
            assertEquals(stockSize, game.players[2].stockPile.size)
            assertEquals(DrawPile.TOTAL_CARD_COUNT - stockSize * 3 - Player.HAND_SIZE, game.drawPile.size)
        }
    }

    internal class PlayingGame {
        @Test
        fun `should end turn when discarding`() {
            val cards = MutableList(20) { Card(1) }
            val game = Game(3, 1, cards)
            assertEquals(game.players[0], game.currentPlayer)
            val card = game.currentPlayer.hand[0]
            val discardPile = game.currentPlayer.discardPiles[0]
            game.discardCard(card, discardPile)
            assertEquals(game.players[1], game.currentPlayer)
            assertEquals(card, discardPile.top())
        }

        @Test
        fun `should finish game when playing last stock pile card`() {
            val cards = MutableList(20) { Card(1) }
            val game = Game(3, 1, cards)
            game.playStockPile(game.buildingPiles[0])
            assertTrue(game.finished, "Game should be finished")
            assertEquals(game.currentPlayer, game.victor)
        }

        @Test
        fun `should keep victor same`() {
            val cards = MutableList(20) { Card(1) }
            val game = Game(3, 1, cards)
            game.playStockPile(game.buildingPiles[0])
            game.discardCard(game.currentPlayer.hand[0], game.currentPlayer.discardPiles[0])
            game.playStockPile(game.buildingPiles[1])
            assertEquals(game.players[0], game.victor)
            assertNotEquals(game.currentPlayer, game.victor)
        }

        @Test
        fun `should draw new hand when playing all cards`() {
            val stockCards = MutableList(3) { Card(12) }
            val firstHand = MutableList(5) { index -> Card(index + 1) }
            val secondHand = MutableList(5) { Card(12) }
            val cards = secondHand + firstHand.reversed() + stockCards
            val game = Game(3, 1, cards.toMutableList())
            assert(game.currentPlayer.hand.containsAll(firstHand)) {
                "Test requires player to have correct cards. Expected $firstHand, current ${game.currentPlayer.hand}"
            }
            for (i in 1..5) game.playHandCard(game.currentPlayer.hand[0], game.buildingPiles[0])
            assertEquals(5, game.currentPlayer.hand.size)
            assertTrue(
                game.buildingPiles[0].cards.containsAll(firstHand),
                "All firstHand cards should be in buildingPile"
            )
        }

        @Test
        fun `should be able to play discarded card`() {
            val cards = MutableList(20) { Card(1) }
            val game = Game(1, 1, cards)
            val card = game.currentPlayer.hand[0]
            game.discardCard(game.currentPlayer.hand[0], game.currentPlayer.discardPiles[0])
            assertEquals(card, game.currentPlayer.discardPiles[0].top())
            game.playDiscardPileCard(game.currentPlayer.discardPiles[0], game.buildingPiles[0])
            assertEquals(card, game.buildingPiles[0].top())
        }

        @Test
        fun `should have full hand when starting turn`() {
            val cards = MutableList(20) { Card(1) }
            val game = Game(1, 1, cards)
            game.playHandCard(game.currentPlayer.hand[0], game.buildingPiles[0])
            assertEquals(4, game.currentPlayer.hand.size)
            game.discardCard(game.currentPlayer.hand[0], game.currentPlayer.discardPiles[0])
            assertEquals(5, game.currentPlayer.hand.size)
        }
    }
}
