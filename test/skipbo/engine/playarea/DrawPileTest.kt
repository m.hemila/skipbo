package skipbo.engine.playarea

import org.junit.jupiter.api.Test
import skipbo.engine.Card
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

internal class DrawPileTest {
    @Test
    fun `should draw card`() {
        val card = Card(1)
        val test = mutableListOf<Card>()
        test.add(card)
        val drawPile = DrawPile(RemovedPile(), test)
        assertEquals(card, drawPile.draw())
        assertTrue(drawPile.isEmpty(), "Should remove the card when drawn")
    }

    @Test
    fun `should add removed cards back if draw pile is empty`() {
        val card = Card(1)
        val test = mutableListOf<Card>()
        test.add(card)
        val discarded = RemovedPile(test)
        val drawPile = DrawPile(discarded, mutableListOf())
        assertEquals(card, drawPile.draw())
    }

    @Test
    fun `should shuffle removed cards when adding`() {
        val test = MutableList(Card.HIGHEST_CARD + 1) { index -> Card(index) }
        val original = test.toList()
        val reversed = test.toList().reversed()
        val discarded = RemovedPile(test)
        val drawPile = DrawPile(discarded, mutableListOf())
        val actual = mutableListOf<Card>()
        for (i in 1..reversed.size) {
            actual.add(drawPile.draw())
        }
        assertNotEquals(original, actual)
        assertNotEquals(reversed, actual)
    }

    @Test
    fun `should shuffle when created`() {
        val drawPile1 = DrawPile(RemovedPile())
        val drawPile2 = DrawPile(RemovedPile())
        assertNotEquals(drawPile1.drawStockPile(100), drawPile2.drawStockPile(100))
    }
}
