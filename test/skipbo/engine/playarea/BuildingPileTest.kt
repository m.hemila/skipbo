package skipbo.engine.playarea

import org.junit.jupiter.api.Test
import skipbo.engine.Card
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class BuildingPileTest {
    @Test
    fun `should be able to play all cards`() {
        val removed = RemovedPile()
        val pile = BuildingPile(removed)
        for (i in 1..Card.HIGHEST_CARD) {
            pile.play(Card(i))
        }
    }

    @Test
    fun `should be able to play joker`() {
        val removed = RemovedPile()
        val pile = BuildingPile(removed)
        for (i in 1..Card.HIGHEST_CARD) {
            pile.play(Card.joker())
        }
    }

    @Test
    fun `should be remove cards after building full stack`() {
        val cards: List<Card> = List(Card.HIGHEST_CARD) { index -> Card(index + 1) }
        val removed = RemovedPile()
        val pile = BuildingPile(removed)
        for (card in cards) {
            pile.play(card)
        }
        assertTrue(removed.cards.containsAll(cards), "Removed cards did not contain all original cards")
        assertEquals(1, pile.nextNumber)
        assertTrue(pile.isEmpty(), "BuildingPile should be empty after finishing full stack")
    }

    @Test
    fun `should do no changes if card is wrong`() {
        val card = Card(Card.HIGHEST_CARD)
        val removed = RemovedPile()
        val pile = BuildingPile(removed)
        assertFalse(pile.play(card), "Should return false on wrong card")
        assertEquals(1, pile.nextNumber)
        assertTrue(pile.isEmpty(), "BuildingPile should be empty when play failed")
    }
}
