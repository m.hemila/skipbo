package skipbo.engine.player

import org.junit.jupiter.api.Test
import skipbo.engine.Card
import skipbo.engine.playarea.DrawPile
import skipbo.engine.playarea.RemovedPile
import kotlin.test.assertEquals

internal class PlayerTest {
    @Test
    fun `should draw to full hand size hand`() {
        val test = MutableList(13) { index -> Card(index) }
        val drawPile = DrawPile(RemovedPile(), test)
        val player = Player(StockPile(mutableListOf()))
        player.drawHand(drawPile)
        assertEquals(Player.HAND_SIZE, player.hand.size)
    }
}
