# A Collection project for different skipbo projects

Currently, only single project exists: skipbo.engine. 
There is a plan to create a 

#### TODO
- Add license

## skipbo.engine

A core project for simulating a skipbo game.

### Possible improvements
- Currently, cards can be removed and added separately. 
  This means there is a possible issue that when removing card, 
  it is not added to another location.
  This would lead to "leaking cards" out of the game.
  To solve this a more structured approach could be used, 
  where moving card is a single action with both source and destination.

- Make more(/fully) functional version of the game. 
  This could be combined with above so that once game is created
  it is not mutated, but instead actions are piled on top of each other.
  In reality, there is not much reason for making a functional version
  of the game except practice. 
