package skipbo.engine

import skipbo.engine.playarea.BuildingPile
import skipbo.engine.playarea.DrawPile
import skipbo.engine.playarea.RemovedPile
import skipbo.engine.player.DiscardPile
import skipbo.engine.player.Player

public class Game internal constructor(players: Int, stockSize: Int, cards: MutableList<Card>) {
    public constructor(players: Int, stockSize: Int) : this(players, stockSize, DrawPile.newDeck())

    init {
        require(players > 0) { "Game has to have players" }
        require(stockSize > 0) { "Game requires stockSize to be more than 0" }
        require(players * stockSize < DrawPile.TOTAL_CARD_COUNT) { "Not enough cards in game for all players" }
    }

    public val removedPile: RemovedPile = RemovedPile()
    public val drawPile: DrawPile = DrawPile(removedPile, cards)
    public val players: List<Player> = List(players) { Player(drawPile.drawStockPile(stockSize)) }
    public val buildingPiles: List<BuildingPile> = List(BUILDING_PILE_COUNT) { BuildingPile(removedPile) }
    private var currentPlayerIndex: Int = 0
    public var victor: Player? = null
        private set
    public val finished: Boolean get() = victor != null

    init {
        currentPlayer.drawHand(drawPile)
    }

    public val currentPlayer: Player get() = players[currentPlayerIndex]

    public fun playStockPile(buildingPile: BuildingPile): Boolean {
        val card = currentPlayer.stockPile.top()
        return if (card != null && buildingPile.play(card)) {
            currentPlayer.stockPile.removeTop()
            if (victor == null && currentPlayer.stockPile.isEmpty()) {
                victor = currentPlayer
            }
            true
        } else {
            false
        }
    }

    public fun playHandCard(card: Card, buildingPile: BuildingPile): Boolean {
        require(currentPlayer.hand.contains(card)) { "No such card in player's hand" }
        return if (currentPlayer.remove(card)) {
            buildingPile.play(card)
            if (currentPlayer.hand.isEmpty()) currentPlayer.drawHand(drawPile)
            true
        } else {
            false
        }
    }

    public fun playDiscardPileCard(discardPile: DiscardPile, buildingPile: BuildingPile): Boolean {
        val card = discardPile.top()
        return if (card != null && buildingPile.play(card)) {
            discardPile.removeTop()
            true
        } else {
            false
        }
    }

    public fun discardCard(card: Card, discardPile: DiscardPile): Player {
        require(currentPlayer.hand.contains(card)) { "No such card in player's hand" }
        if (currentPlayer.remove(card)) discardPile.discard(card)
        nextPLayer()
        return currentPlayer
    }

    private fun nextPLayer() {
        if (currentPlayerIndex == players.lastIndex) currentPlayerIndex = 0
        else currentPlayerIndex += 1
        currentPlayer.drawHand(drawPile)
    }

    public companion object {
        public const val BUILDING_PILE_COUNT: Int = 4
    }
}
