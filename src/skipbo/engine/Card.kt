package skipbo.engine

// Skipbo cards are 1 - 12 and 12 of each as well as 18 of "skipbo" cards that act as jokers.
// Here we implement "skipbo" cards as number 0.
public class Card internal constructor(public val number: Int) {
    public val isJoker: Boolean = number == JOKER_NUMBER

    public override fun toString(): String {
        return number.toString()
    }

    public companion object {
        public const val JOKER_NUMBER: Int = 0
        public const val HIGHEST_CARD: Int = 12

        internal fun joker(): Card = Card(JOKER_NUMBER)
    }
}
