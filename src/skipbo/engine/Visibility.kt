package skipbo.engine

public interface TopVisible {
    // Returns top card of pile
    public fun top(): Card?
}

public interface AllVisible : TopVisible {
    // Returns cards of pile from top to bottom
    public val cards: List<Card>
}
