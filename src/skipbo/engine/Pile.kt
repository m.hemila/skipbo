package skipbo.engine

public abstract class Pile(protected val pile: MutableList<Card>) {
    public val size: Int get() = pile.size
    public fun isEmpty(): Boolean = pile.isEmpty()
    public fun isNotEmpty(): Boolean = pile.isNotEmpty()
}
