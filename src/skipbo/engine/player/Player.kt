package skipbo.engine.player

import skipbo.engine.Card
import skipbo.engine.playarea.DrawPile

public class Player internal constructor(public val stockPile: StockPile) {
    public val discardPiles: List<DiscardPile> = List(DISCARD_PILE_COUNT) { DiscardPile() }
    private val _hand: MutableList<Card> = mutableListOf()
    public val hand: List<Card> get() = _hand.toList()

    internal fun drawHand(drawPile: DrawPile) {
        while (hand.size < HAND_SIZE) {
            _hand.add(drawPile.draw())
        }
    }

    internal fun remove(card: Card): Boolean = _hand.remove(card)

    public companion object {
        public const val HAND_SIZE: Int = 5
        public const val DISCARD_PILE_COUNT: Int = 4
    }
}
