package skipbo.engine.player

import skipbo.engine.Card
import skipbo.engine.Pile
import skipbo.engine.TopVisible

public class StockPile internal constructor(pile: MutableList<Card>) : Pile(pile), TopVisible {
    public override fun top(): Card? = pile.lastOrNull()
    internal fun removeTop() = pile.removeLast()
}
