package skipbo.engine.player

import skipbo.engine.AllVisible
import skipbo.engine.Card
import skipbo.engine.Pile

public class DiscardPile internal constructor(pile: MutableList<Card> = mutableListOf()) : Pile(pile), AllVisible {
    // Ordered top to bottom
    public override val cards: List<Card> get() = pile.toList().reversed()
    public override fun top(): Card? = pile.lastOrNull()
    internal fun removeTop() = pile.removeLast()
    internal fun discard(card: Card) {
        pile.add(card)
    }
}
