package skipbo.engine.playarea

import skipbo.engine.Card
import skipbo.engine.Pile
import skipbo.engine.player.StockPile

public class DrawPile internal constructor(
    private val removedPile: RemovedPile,
    pile: MutableList<Card>,
) : Pile(pile) {
    internal constructor(removedPile: RemovedPile) : this(removedPile, newDeck())

    internal fun drawStockPile(size: Int): StockPile {
        require(size <= pile.size) { "Run out of cards when drawing stock piles" }
        val ret = pile.takeLast(size)
        pile.removeAll(ret)
        return StockPile(ret.toMutableList())
    }

    internal fun draw(): Card {
        if (pile.isEmpty()) {
            check(removedPile.isNotEmpty()) { "Game has run out of cards, cannot proceed" }
            pile.addAll(removedPile.returnToGame())
        }

        return pile.removeLast()
    }

    public companion object {
        public const val NORMAL_CARD_COUNT: Int = 12
        public const val JOKER_COUNT: Int = 18
        public const val TOTAL_CARD_COUNT: Int = NORMAL_CARD_COUNT * Card.HIGHEST_CARD + JOKER_COUNT

        internal fun newDeck(): MutableList<Card> {
            val ret = mutableListOf<Card>()
            for (i in 1..Card.HIGHEST_CARD) {
                ret.addAll(Array(NORMAL_CARD_COUNT) { Card(i) })
            }
            ret.addAll(Array(JOKER_COUNT) { Card.joker() })
            assert(ret.size == TOTAL_CARD_COUNT) { "Wrong number of cards, expected $TOTAL_CARD_COUNT actual ${ret.size}" }
            ret.shuffle()
            return ret
        }
    }
}
