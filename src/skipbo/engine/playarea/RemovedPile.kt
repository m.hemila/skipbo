package skipbo.engine.playarea

import skipbo.engine.AllVisible
import skipbo.engine.Card
import skipbo.engine.Pile

public class RemovedPile internal constructor(pile: MutableList<Card> = mutableListOf()) : Pile(pile), AllVisible {
    // Ordered top to bottom
    override fun top(): Card? = pile.lastOrNull()
    public override val cards: List<Card> get() = pile.toList().reversed()

    internal fun remove(cards: List<Card>) {
        pile.addAll(cards)
    }

    internal fun returnToGame(): List<Card> {
        val ret = pile.toList().shuffled()
        pile.clear()
        return ret
    }
}
