package skipbo.engine.playarea

import skipbo.engine.AllVisible
import skipbo.engine.Card
import skipbo.engine.Pile

public class BuildingPile internal constructor(
    private val removedPile: RemovedPile,
    pile: MutableList<Card> = mutableListOf(),
) : Pile(pile), AllVisible {
    public var nextNumber: Int = 1
        private set

    // Ordered top to bottom
    public override val cards: List<Card> get() = pile.toList().reversed()
    public override fun top(): Card? = pile.lastOrNull()

    internal fun play(card: Card): Boolean {
        return if (card.number == nextNumber || card.isJoker) {
            pile.add(card)
            nextNumber += 1
            if (nextNumber > Card.HIGHEST_CARD) removeCompletedPile()
            true
        } else {
            false
        }
    }

    private fun removeCompletedPile() {
        removedPile.remove(pile)
        pile.clear()
        nextNumber = 1
    }
}
