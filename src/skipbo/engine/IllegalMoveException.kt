package skipbo.engine

public class IllegalMoveException(msg: String) : Exception(msg)
